/*
 * lines2d.cc
 *
 *  Created on: Feb 26, 2014
 *      Author: VregNamikaze
 */

#include "lines2d.h"

Color::Color() :
	blue(0), green(0), red(0)
{
}
Color::Color(uint8_t r, uint8_t g, uint8_t b) :
	blue(b), green(g), red(r)
{
}
Color::~Color()
{
}

Point2D::Point2D(double x, double y)
{
	this->x = x;
	this->y = y;
}

Point2D::Point2D(double x, double y, double z)
{
	this->x = x;
	this->y = y;
        this->z = z;
}

Line2D::Line2D(Point2D& p1, Point2D& p2, Color& color)
{
	this->p1 = p1;
	this->p2 = p2;
	this->color = color;
}