/*
 * EasyImage.cc
 * Copyright (C) 2011  Daniel van den Akker
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "EasyImage.h"
#include "../math.h"
#include <algorithm>
#include <assert.h>
#include <math.h>
#include <limits>
#include <stdint.h>

#define le32toh(x) (x)

namespace
{
	//structs borrowed from wikipedia's article on the BMP file format
	struct bmpfile_magic
	{
			uint8_t magic[2];
	};

	struct bmpfile_header
	{
			uint32_t file_size;
			uint16_t reserved_1;
			uint16_t reserved_2;
			uint32_t bmp_offset;
	};
	struct bmp_header
	{
			uint32_t header_size;
			int32_t width;
			int32_t height;
			uint16_t nplanes;
			uint16_t bits_per_pixel;
			uint32_t compress_type;
			uint32_t pixel_size;
			int32_t hres;
			int32_t vres;
			uint32_t ncolors;
			uint32_t nimpcolors;
	};
	//copy-pasted from lparser.cc to allow these classes to be used independently from each other
	class enable_exceptions
	{
		private:
			std::ios& ios;
			std::ios::iostate state;
		public:
			enable_exceptions(std::ios& an_ios, std::ios::iostate exceptions) :
				ios(an_ios)
			{
				state = ios.exceptions();
				ios.exceptions(exceptions);
			}
			~enable_exceptions()
			{
				ios.exceptions(state);
			}
	};
	//helper function to convert a number (char, int, ...) to little endian
	//regardless of the endiannes of the system
	//more efficient machine-dependent functions exist, but this one is more portable
	template<typename T> T to_little_endian(T value)
	{
		//yes, unions must be used with caution, but this is a case in which a union is needed
		union
		{
				T t;
				uint8_t bytes[sizeof(T)];
		} temp_storage;

		for (uint8_t i = 0; i < sizeof(T); i++)
		{
			temp_storage.bytes[i] = value & 0xFF;
			value >>= 8;
		}
		return temp_storage.t;
	}

	template<typename T> T from_little_endian(T value)
	{
		//yes, unions must be used with caution, but this is a case in which a union is needed
		union
		{
				T t;
				uint8_t bytes[sizeof(T)];
		} temp_storage;
		temp_storage.t = value;
		T retVal = 0;

		for (uint8_t i = 0; i < sizeof(T); i++)
		{
			retVal = (retVal << 8) | temp_storage.bytes[sizeof(T) - i - 1];
		}
		return retVal;
	}

}

img::UnsupportedFileTypeException::UnsupportedFileTypeException(std::string const& msg) :
	message(msg)
{
}
img::UnsupportedFileTypeException::UnsupportedFileTypeException(const UnsupportedFileTypeException &original)
: std::exception(original)
, message(original.message)
{
}
img::UnsupportedFileTypeException::~UnsupportedFileTypeException() throw ()
{
}
img::UnsupportedFileTypeException& img::UnsupportedFileTypeException::operator=(UnsupportedFileTypeException const& original)
{
	this->message = original.message;
	return *this;
}
const char* img::UnsupportedFileTypeException::what() const throw ()
{
	return message.c_str();
}

img::EasyImage::EasyImage() :
	width(0), height(0), bitmap()
{
}

img::EasyImage::EasyImage(unsigned int _width, unsigned int _height, Color color) :
	width(_width), height(_height), bitmap(width * height, color)
{
}

img::EasyImage::EasyImage(EasyImage const& img) :
	width(img.width), height(img.height), bitmap(img.bitmap)
{
}

img::EasyImage::~EasyImage()
{
	bitmap.clear();
}

img::EasyImage& img::EasyImage::operator=(img::EasyImage const& img)
{
	width = img.width;
	height = img.height;
	bitmap.assign(img.bitmap.begin(),img.bitmap.end());
	return (*this);
}

unsigned int img::EasyImage::get_width() const
{
	return width;
}

unsigned int img::EasyImage::get_height() const
{
	return height;
}

void img::EasyImage::clear(Color color)
{
	for (std::vector<Color>::iterator i = bitmap.begin(); i != bitmap.end(); i++)
	{
		*i = color;
	}
}

Color& img::EasyImage::operator()(unsigned int x, unsigned int y)
{
	assert(x < this->width);
	assert(y < this->height);
	assert(0 <= x);
	assert(0 <= y);
	return bitmap.at(x * height + y);
}

Color const& img::EasyImage::operator()(unsigned int x, unsigned int y) const
{
	assert(x < this->width);
	assert(y < this->height);
	return bitmap.at(x * height + y);
}

void img::EasyImage::draw_line(Line2D& line)
{
    unsigned int p1_x = Math::doubleToUInt(line.p1.x);
    unsigned int p1_y = Math::doubleToUInt(line.p1.y);
    unsigned int p2_x = Math::doubleToUInt(line.p2.x);
    unsigned int p2_y = Math::doubleToUInt(line.p2.y);
    
    assert(p1_x < this->width && p1_y < this->height);
    assert(p2_x < this->width && p2_y < this->height);

    if (p1_x == p2_x)
    {
        //special case for p1_x == p2_x
        for (unsigned int i = std::min(p1_y, p2_y); i <= std::max(p1_y, p2_y); i++)
        {
            (*this)(p1_x, i) = line.color;
        }
    }
    else if (p1_y == p2_y)
    {
        //special case for p1_y == p2_y
        for (unsigned int i = std::min(p1_x, p2_x); i <= std::max(p1_x, p2_x); i++)
        {
                (*this)(i, p1_y) = line.color;
        }
    }
    else
    {
        if (p1_x > p2_x)
        {
            //flip points if p2_x>p1_x: we want p1_x to have the lowest value
            std::swap(p1_x, p2_x);
            std::swap(p1_y, p2_y);
        }
        
        double m = ((double) p2_y - (double) p1_y) / ((double) p2_x - (double) p1_x);
        
        if (-1.0 <= m && m <= 1.0)
        {
            for (unsigned int i = 0; i <= (p2_x - p1_x); i++)
            {
                (*this)(p1_x + i, (unsigned int)round(p1_y + m * i)) = line.color;
            }
        }
        else if (m > 1.0)
        {
            for (unsigned int i = 0; i <= (p2_y - p1_y); i++)
            {
                (*this)((unsigned int)round(p1_x + (i / m)), p1_y + i) = line.color;
            }
        }
        else if (m < -1.0)
        {
            for (unsigned int i = 0; i <= (p1_y - p2_y); i++)
            {
                (*this)((unsigned int)round(p1_x - (i / m)), p1_y - i) = line.color;
            }
        }
    }
}

void img::EasyImage::draw_zbuff_line(Line2D& line, ZBuffer& zBuffer)
{
    unsigned int p1_x = Math::doubleToInt(line.p1.x);
    unsigned int p1_y = Math::doubleToInt(line.p1.y);
    unsigned int p2_x = Math::doubleToInt(line.p2.x);
    unsigned int p2_y = Math::doubleToInt(line.p2.y);
    
    assert(p1_x < this->width && p1_y < this->height);
    assert(p2_x < this->width && p2_y < this->height);

    if (p1_x == p2_x && p1_y == p2_y)
    {
        //special case for p1_x == p2_x && p1_y == p2_y

        (*this)(p1_x, p1_y) = line.color;
        zBuffer[p1_y][p1_x] = 1 / line.p1.z;
    }
    else if (p1_x == p2_x)
    {
        //special case for p1_x == p2_x

        unsigned int a = std::max(p1_y, p2_y) - std::min(p1_y, p2_y);
        unsigned int j = 0;

        if (p1_y > p2_y)
        {
            //swap line.p1.z and line.p2.z when p1_y > p2_y
            std::swap(line.p1.z, line.p2.z);
        }
        
        for (unsigned int i = std::min(p1_y, p2_y); i <= std::max(p1_y, p2_y); i++)
        {
            double zInv = (1 - j / a) / line.p1.z + (j / a) / line.p2.z;
            if (zInv < zBuffer[i][p1_x])
            {
                (*this)(p1_x, i) = line.color;
                zBuffer[i][p1_x] = zInv;
            }
            j++;
        }
    }
    else if (p1_y == p2_y)
    {
        //special case for p1_y == p2_y

        unsigned int a = std::max(p1_x, p2_x) - std::min(p1_x, p2_x);
        unsigned int j = 0;

        if (p1_x > p2_x)
        {
            //swap line.p1.z and line.p2.z when p1_x > p2_x
            std::swap(line.p1.z, line.p2.z);
        }
        
        for (unsigned int i = std::min(p1_x, p2_x); i <= std::max(p1_x, p2_x); i++)
        {
            double zInv = (1 - j / a) / line.p1.z + (j / a) / line.p2.z;
            if (zInv < zBuffer[p1_y][i])
            {
                (*this)(i, p1_y) = line.color;
                zBuffer[p1_y][i] = zInv;
            }
            j++;
        }
    }
    else
    {
        if (p1_x > p2_x)
        {
            //flip points if p2_x>p1_x: we want p1_x to have the lowest value
            std::swap(p1_x, p2_x);
            std::swap(p1_y, p2_y);
            //obviously the z positions need to be flipped as well
            std::swap(line.p1.z, line.p2.z);
        }
        
        double m = ((double) p2_y - (double) p1_y) / ((double) p2_x - (double) p1_x);
        
        if (-1.0 <= m && m <= 1.0)
        {
            unsigned int a = p2_x - p1_x;
            for (unsigned int i = 0; i <= (p2_x - p1_x); i++)
            {
                double zInv = (1 - i / a) / line.p1.z + (i / a) / line.p2.z;
                if (zInv < zBuffer[(unsigned int)round(p1_y + m * i)][p1_x + i])
                {
                    (*this)(p1_x + i, (unsigned int)round(p1_y + m * i)) = line.color;
                    zBuffer[(unsigned int)round(p1_y + m * i)][p1_x + i] = zInv;
                }
            }
        }
        else if (m > 1.0)
        {
            unsigned int a = p2_y - p1_y;
            for (unsigned int i = 0; i <= (p2_y - p1_y); i++)
            {
                double zInv = (1 - i / a) / line.p1.z + (i / a) / line.p2.z;
                if (zInv < zBuffer[p1_y + i][(unsigned int)round(p1_x + (i / m))])
                {
                    (*this)((unsigned int)round(p1_x + (i / m)), p1_y + i) = line.color;
                    zBuffer[p1_y + i][(unsigned int)round(p1_x + (i / m))] = zInv;
                }
            }
        }
        else if (m < -1.0)
        {
            unsigned int a = p1_y - p2_y;
            for (unsigned int i = 0; i <= (p1_y - p2_y); i++)
            {
                double zInv = (1 - i / a) / line.p1.z + (i / a) / line.p2.z;
                if (zInv < zBuffer[p1_y - i][(unsigned int)round(p1_x - (i / m))])
                {
                    (*this)((unsigned int)round(p1_x - (i / m)), p1_y - i) = line.color;
                    zBuffer[p1_y - i][(unsigned int)round(p1_x - (i / m))] = zInv;
                }
            }
        }
    }
}

void img::EasyImage::draw_triag(Triangle& triangle)
{
    assert(triangle.p1.x < this->width && triangle.p1.y < this->height);
    assert(triangle.p2.x < this->width && triangle.p2.y < this->height);
    assert(triangle.p3.x < this->width && triangle.p3.y < this->height);
    
    /*/
    \*\ Scanline.
    /*/ for (unsigned int yi = std::floor(std::min(triangle.proj_p1.y, std::min(triangle.proj_p2.y, triangle.proj_p3.y)) + 1); yi <= std::floor(std::max(triangle.proj_p1.y, std::max(triangle.proj_p2.y, triangle.proj_p3.y))); yi++)
        {
            double xL1, xL2, xL3;
            xL1 = xL2 = xL3 = std::numeric_limits<double>::infinity();
            
            double xR1, xR2, xR3;
            xR1 = xR2 = xR3 = -std::numeric_limits<double>::infinity();
            
            double a = (yi - triangle.proj_p1.y) * (yi - triangle.proj_p2.y);
            double b = (yi - triangle.proj_p2.y) * (yi - triangle.proj_p3.y);
            double c = (yi - triangle.proj_p3.y) * (yi - triangle.proj_p1.y);
            
            if (a <= 0 && triangle.proj_p1.y != triangle.proj_p2.y)
            {
                xL1 = triangle.proj_p2.x + (triangle.proj_p1.x - triangle.proj_p2.x) * (yi - triangle.proj_p2.y) / (triangle.proj_p1.y - triangle.proj_p2.y);
                xR1 = triangle.proj_p2.x + (triangle.proj_p1.x - triangle.proj_p2.x) * (yi - triangle.proj_p2.y) / (triangle.proj_p1.y - triangle.proj_p2.y);
            }
            
            if (b <= 0 && triangle.proj_p2.y != triangle.proj_p3.y)
            {
                xL2 = triangle.proj_p3.x + (triangle.proj_p2.x - triangle.proj_p3.x) * (yi - triangle.proj_p3.y) / (triangle.proj_p2.y - triangle.proj_p3.y);
                xR2 = triangle.proj_p3.x + (triangle.proj_p2.x - triangle.proj_p3.x) * (yi - triangle.proj_p3.y) / (triangle.proj_p2.y - triangle.proj_p3.y);
            }
            
            if (c <= 0 && triangle.proj_p3.y != triangle.proj_p1.y)
            {
                xL3 = triangle.proj_p1.x + (triangle.proj_p3.x - triangle.proj_p1.x) * (yi - triangle.proj_p1.y) / (triangle.proj_p3.y - triangle.proj_p1.y);
                xR3 = triangle.proj_p1.x + (triangle.proj_p3.x - triangle.proj_p1.x) * (yi - triangle.proj_p1.y) / (triangle.proj_p3.y - triangle.proj_p1.y);
            }
            
            unsigned int xL = std::floor(std::min(xL1, std::min(xL2, xL3)) + 1);
            unsigned int xR = std::floor(std::max(xR1, std::max(xR2, xR3)));
            
            /*/
            \*\ Draw zbuffered line.
            /*/ if (xL == xR)
                {
                    (*this)(xL, yi) = triangle.color;
                }
                else
                {
                    for (unsigned int i = xL; i <= xR; ++i)
                    {
                        (*this)(i, yi) = triangle.color;
                    }
                }
        }
}

void img::EasyImage::draw_zbuff_triag(Triangle& triangle, ZBuffer& zBuffer, double d)
{
    assert(triangle.p1.x < this->width && triangle.p1.y < this->height);
    assert(triangle.p2.x < this->width && triangle.p2.y < this->height);
    assert(triangle.p3.x < this->width && triangle.p3.y < this->height);

    // Calculate central values.
    double central_x = (triangle.proj_p1.x + triangle.proj_p2.x + triangle.proj_p3.x) / 3;
    double central_y = (triangle.proj_p1.y + triangle.proj_p2.y + triangle.proj_p3.y) / 3;
    double central_inv_z = 1 / ((triangle.p1.z + triangle.p2.z + triangle.p3.z) / 3) * 1.0001F;
    
    // Get normal vector w of this triangle by taking the cross product of two vectors on this triangle.
    double u1 = (triangle.p1.x - triangle.p2.x); double u2 = (triangle.p1.y - triangle.p2.y); double u3 = (triangle.p1.z - triangle.p2.z);
    double v1 = (triangle.p1.x - triangle.p3.x); double v2 = (triangle.p1.y - triangle.p3.y); double v3 = (triangle.p1.z - triangle.p3.z);

    double w1 = u2 * v3 - u3 * v2;
    double w2 = u3 * v1 - u1 * v3;
    double w3 = u1 * v2 - u2 * v1;
    
    // Take the dot product of normal vector w with any point on this triangle.
    // That will be equal to k.
    double k = w1 * triangle.p1.x + w2 * triangle.p1.y + w3 * triangle.p1.z;
    
    // Calculate dzdx and dzdy.
    double dzdx = w1 / k / -d;
    double dzdy = w2 / k / -d;

    /*/
    \*\ Scanline.
    /*/ for (unsigned int yi = std::floor(std::min(triangle.proj_p1.y, std::min(triangle.proj_p2.y, triangle.proj_p3.y)) + 1); yi <= std::floor(std::max(triangle.proj_p1.y, std::max(triangle.proj_p2.y, triangle.proj_p3.y))); yi++)
        {
            double xL1, xL2, xL3;
            xL1 = xL2 = xL3 = std::numeric_limits<double>::infinity();
            
            double xR1, xR2, xR3;
            xR1 = xR2 = xR3 = -std::numeric_limits<double>::infinity();
            
            double a = (yi - triangle.proj_p1.y) * (yi - triangle.proj_p2.y);
            double b = (yi - triangle.proj_p2.y) * (yi - triangle.proj_p3.y);
            double c = (yi - triangle.proj_p3.y) * (yi - triangle.proj_p1.y);
            
            if (a <= 0 && triangle.proj_p1.y != triangle.proj_p2.y)
            {
                xL1 = triangle.proj_p2.x + (triangle.proj_p1.x - triangle.proj_p2.x) * (yi - triangle.proj_p2.y) / (triangle.proj_p1.y - triangle.proj_p2.y);
                xR1 = triangle.proj_p2.x + (triangle.proj_p1.x - triangle.proj_p2.x) * (yi - triangle.proj_p2.y) / (triangle.proj_p1.y - triangle.proj_p2.y);
            }
            
            if (b <= 0 && triangle.proj_p2.y != triangle.proj_p3.y)
            {
                xL2 = triangle.proj_p3.x + (triangle.proj_p2.x - triangle.proj_p3.x) * (yi - triangle.proj_p3.y) / (triangle.proj_p2.y - triangle.proj_p3.y);
                xR2 = triangle.proj_p3.x + (triangle.proj_p2.x - triangle.proj_p3.x) * (yi - triangle.proj_p3.y) / (triangle.proj_p2.y - triangle.proj_p3.y);
            }
            
            if (c <= 0 && triangle.proj_p3.y != triangle.proj_p1.y)
            {
                xL3 = triangle.proj_p1.x + (triangle.proj_p3.x - triangle.proj_p1.x) * (yi - triangle.proj_p1.y) / (triangle.proj_p3.y - triangle.proj_p1.y);
                xR3 = triangle.proj_p1.x + (triangle.proj_p3.x - triangle.proj_p1.x) * (yi - triangle.proj_p1.y) / (triangle.proj_p3.y - triangle.proj_p1.y);
            }
            
            unsigned int xL = std::floor(std::min(xL1, std::min(xL2, xL3)) + 1);
            unsigned int xR = std::floor(std::max(xR1, std::max(xR2, xR3)));
            
            /*/
            \*\ Draw zbuffered line.
            /*/ if (xL == xR)
                {
                    double inv_z = central_inv_z + dzdx * (xL - central_x) + dzdy * (yi - central_y);
                    if (inv_z < zBuffer[yi][xL])
                    {
                        (*this)(xL, yi) = triangle.color;
                        zBuffer[yi][xL] = inv_z;
                    }
                }
                else
                {
                    for (unsigned int i = xL; i <= xR; ++i)
                    {
                        double inv_z = central_inv_z + dzdx * (i - central_x) + dzdy * (yi - central_y);
                        if (inv_z < zBuffer[yi][i])
                        {
                            (*this)(i, yi) = triangle.color;
                            zBuffer[yi][i] = inv_z;
                        }
                    }
                }
        }
}

std::ostream& img::operator<<(std::ostream& out, EasyImage const& image)
{

	//temporaryily enable exceptions on output stream
	enable_exceptions(out, std::ios::badbit | std::ios::failbit);
	//declare some struct-vars we're going to need:
	bmpfile_magic magic;
	bmpfile_header file_header;
	bmp_header header;
	uint8_t padding[] =
	{ 0, 0, 0, 0 };
	//calculate the total size of the pixel data
	unsigned int line_width = image.get_width() * 3; //3 bytes per pixel
	unsigned int line_padding = 0;
	if (line_width % 4 != 0)
	{
		line_padding = 4 - (line_width % 4);
	}
	//lines must be aligned to a multiple of 4 bytes
	line_width += line_padding;
	unsigned int pixel_size = image.get_height() * line_width;

	//start filling the headers
	magic.magic[0] = 'B';
	magic.magic[1] = 'M';

	file_header.file_size = to_little_endian(pixel_size + sizeof(file_header) + sizeof(header) + sizeof(magic));
	file_header.bmp_offset = to_little_endian(sizeof(file_header) + sizeof(header) + sizeof(magic));
	file_header.reserved_1 = 0;
	file_header.reserved_2 = 0;
	header.header_size = to_little_endian(sizeof(header));
	header.width = to_little_endian(image.get_width());
	header.height = to_little_endian(image.get_height());
	header.nplanes = to_little_endian(1);
	header.bits_per_pixel = to_little_endian(24);//3bytes or 24 bits per pixel
	header.compress_type = 0; //no compression
	header.pixel_size = pixel_size;
	header.hres = to_little_endian(11811); //11811 pixels/meter or 300dpi
	header.vres = to_little_endian(11811); //11811 pixels/meter or 300dpi
	header.ncolors = 0; //no color palette
	header.nimpcolors = 0;//no important colors

	//okay that should be all the header stuff: let's write it to the stream
	out.write((char*) &magic, sizeof(magic));
	out.write((char*) &file_header, sizeof(file_header));
	out.write((char*) &header, sizeof(header));

	//okay let's write the pixels themselves:
	//they are arranged left->right, bottom->top, b,g,r
	for (unsigned int i = 0; i < image.get_height(); i++)
	{
		//loop over all lines
		for (unsigned int j = 0; j < image.get_width(); j++)
		{
			//loop over all pixels in a line
			//we cast &color to char*. since the color fields are ordered blue,green,red they should be written automatically
			//in the right order
			out.write((char*) &image(j, i), 3 * sizeof(uint8_t));
		}
		if (line_padding > 0)
			out.write((char*) padding, line_padding);
	}
	//okay we should be done
	return out;
}
std::istream& img::operator>>(std::istream& in, EasyImage & image)
{
	enable_exceptions(in, std::ios::badbit | std::ios::failbit);
	//declare some struct-vars we're going to need
	bmpfile_magic magic;
	bmpfile_header file_header;
	bmp_header header;
	//a temp buffer for reading the padding at the end of each line
	uint8_t padding[] =
	{ 0, 0, 0, 0 };

	//read the headers && do some sanity checks
	in.read((char*) &magic, sizeof(magic));
	if (magic.magic[0] != 'B' || magic.magic[1] != 'M')
		throw UnsupportedFileTypeException("Could not parse BMP File: invalid magic header");
	in.read((char*) &file_header, sizeof(file_header));
	in.read((char*) &header, sizeof(header));
	if (le32toh(header.pixel_size) + le32toh(file_header.bmp_offset) != le32toh(file_header.file_size))
		throw UnsupportedFileTypeException("Could not parse BMP File: file size mismatch");
	if (le32toh(header.header_size) != sizeof(header))
		throw UnsupportedFileTypeException("Could not parse BMP File: Unsupported BITMAPV5HEADER size");
	if (le32toh(header.compress_type) != 0)
		throw UnsupportedFileTypeException("Could not parse BMP File: Only uncompressed BMP files can be parsed");
	if (le32toh(header.nplanes) != 1)
		throw UnsupportedFileTypeException("Could not parse BMP File: Only one plane should exist in the BMP file");
	if (le32toh(header.bits_per_pixel) != 24)
		throw UnsupportedFileTypeException("Could not parse BMP File: Only 24bit/pixel BMP's are supported");
	//if height<0 -> read top to bottom instead of bottom to top
	bool invertedLines = from_little_endian(header.height) < 0;
	image.height = std::abs(from_little_endian(header.height));
	image.width = std::abs(from_little_endian(header.width));
	unsigned int line_padding = from_little_endian(header.pixel_size) / image.height - (3 * image.width);
	//re-initialize the image bitmap
	image.bitmap.clear();
	image.bitmap.assign(image.height * image.width, Color());
	//okay let's read the pixels themselves:
	//they are arranged left->right., bottom->top if height>0, top->bottom if height<0, b,g,r
	for (unsigned int i = 0; i < image.get_height(); i++)
	{
		//loop over all lines
		for (unsigned int j = 0; j < image.get_width(); j++)
		{
			//loop over all pixels in a line
			//we cast &color to char*. since the color fields are ordered blue,green,red, the data read should be written in the right variables
			if (invertedLines)
			{
				//store top-to-bottom
				in.read((char*) &image(j, image.height - 1 - i), 3 * sizeof(uint8_t));
			}
			else
			{
				//store bottom-to-top
				in.read((char*) &image(j, i), 3 * sizeof(uint8_t));
			}
		}
		if (line_padding > 0)
		{
			in.read((char*) padding, line_padding);
		}
	}
	//okay we're done
	return in;
}
