/* 
 * File:   lines3d.cc
 * Author: Vreg
 *
 * Created on 8 maart 2014, 20:35
 */

#include <cmath>
#include <limits>
#include "lines3d.h"

Figure::Figure(Color color)
{
    this->color = color;
}

void Figure::scaleFigure(double scale)
{
    Matrix scaleMatrix;
    
    scaleMatrix(1, 1) = scale;
    scaleMatrix(2, 2) = scale;
    scaleMatrix(3, 3) = scale;
    scaleMatrix(4, 4) = 1;
    
    this->applyTransformation(scaleMatrix);
}

void Figure::rotateX(double angle)
{
    Matrix rotateMatrix;
    
    rotateMatrix(1, 1) = 1;
    rotateMatrix(2, 2) = cos(angle);
    rotateMatrix(2, 3) = sin(angle);
    rotateMatrix(3, 2) = -sin(angle);
    rotateMatrix(3, 3) = cos(angle);
    rotateMatrix(4, 4) = 1;
    
    this->applyTransformation(rotateMatrix);
}

void Figure::rotateY(double angle)
{
    Matrix rotateMatrix;
    
    rotateMatrix(1, 1) = cos(angle);
    rotateMatrix(1, 3) = -sin(angle);
    rotateMatrix(2, 2) = 1;
    rotateMatrix(3, 1) = sin(angle);
    rotateMatrix(3, 3) = cos(angle);
    rotateMatrix(4, 4) = 1;
    
    this->applyTransformation(rotateMatrix);
}

void Figure::rotateZ(double angle)
{
    Matrix rotateMatrix;
    
    rotateMatrix(1, 1) = cos(angle);
    rotateMatrix(1, 2) = sin(angle);
    rotateMatrix(2, 1) = -sin(angle);
    rotateMatrix(2, 2) = cos(angle);
    rotateMatrix(3, 3) = 1;
    rotateMatrix(4, 4) = 1;
    
    this->applyTransformation(rotateMatrix);
}

void Figure::translate(Vector3D vector)
{
    Matrix translateMatrix;
    
    translateMatrix(1, 1) = 1;
    translateMatrix(2, 2) = 1;
    translateMatrix(3, 3) = 1;
    translateMatrix(4, 1) = vector.x;
    translateMatrix(4, 2) = vector.y;
    translateMatrix(4, 3) = vector.z;
    translateMatrix(4, 4) = 1;
    
    this->applyTransformation(translateMatrix);
}

void Figure::eyePointTransform(Vector3D eyePoint)
{
    double radius;
    double xAngle;
    double zAngle;
    
    Figure::toPolar(eyePoint, radius, xAngle, zAngle);
    
    Matrix eyePointMatrix;
    
    eyePointMatrix(1, 1) = -sin(zAngle);
    eyePointMatrix(1, 2) = -cos(zAngle) * cos(xAngle);
    eyePointMatrix(1, 3) = cos(zAngle) * sin(xAngle);
    eyePointMatrix(2, 1) = cos(zAngle);
    eyePointMatrix(2, 2) = -sin(zAngle) * cos(xAngle);
    eyePointMatrix(2, 3) = sin(zAngle) * sin(xAngle);
    eyePointMatrix(3, 2) = sin(xAngle);
    eyePointMatrix(3, 3) = cos(xAngle);
    eyePointMatrix(4, 3) = -radius;
    eyePointMatrix(4, 4) = 1;
    
    this->applyTransformation(eyePointMatrix);
}

void Figure::applyTransformation(const Matrix& trans)
{
    for (unsigned int i = 0; i < this->points.size(); i++)
    {
        this->points[i] *= trans;
    }
}

void Figure::toPolar(Vector3D& point, double& radius, double& xAngle, double& zAngle)
{
    radius = sqrt(pow(point.x, 2) + pow(point.y, 2) + pow(point.z, 2));
    xAngle = acos(point.z / radius);
    zAngle = atan2(point.y, point.x);
}

Point2D Figure::projectPoint(Vector3D& point, double d, bool zBuff)
{
    if (zBuff)
    {
        return Point2D(-d * point.x / point.z, -d * point.y / point.z, point.z);
    }
    else
    {
        return Point2D(-d * point.x / point.z, -d * point.y / point.z);
    }
}

Triangle::Triangle(Vector3D& p1, Vector3D& p2, Vector3D& p3, Color& color)
{
	this->p1 = p1;
	this->p2 = p2;
	this->p3 = p3;
	this->color = color;
}

void Triangle::projectTriangle(double d, bool zBuff)
{
    this->proj_p1 = Figure::projectPoint(p1, d , zBuff);
    this->proj_p2 = Figure::projectPoint(p2, d , zBuff);
    this->proj_p3 = Figure::projectPoint(p3, d , zBuff);
}