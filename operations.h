/*
 * operations.h
 *
 *  Created on: Feb 20, 2014
 *      Author: VregNamikaze
 */

#ifndef OPERATIONS_H_
#define OPERATIONS_H_

#include "utils/EasyImage.h"
#include "utils/ini_configuration.hh"
#include "lines2d.h"
#include "lines3d.h"
#include "utils/lparser.h"
#include <fstream>
#include <sstream>

typedef std::vector<Line2D> Lines2D;
typedef std::vector<Triangle> Triangles;
typedef std::vector<Figure> Figures;

class EngineOperations
{
private:
    // Called by render3D.
    static void linedrawing(Figure& figure, const ini::Configuration& config, std::stringstream& figureName);
    static void generateCube(Figure& figure);
    static void generateTetrahedron(Figure& figure);
    static void generateOctahedron(Figure& figure);
    static void generateIcosahedron(Figure& figure);
    static void generateBuckyBall(Figure& figure);
    static void generateDodecahedron(Figure& figure);
    static void generateSphere(Figure& figure, int n);
    static void generateCone(Figure& figure, int n, double height);
    static void generateCylinder(Figure& figure, int n, double height);
    static void generateTorus(Figure& figure, double r, double R, int n, int m);
    static void lSystem3D(Figure& figure, LParser::LSystem3D& lSystem3D);
    static void generateFractal(Figure& figure, Figures& fractal, int iterations, double scale);
    static void generateMengerSponge(Figure& figure, Figures& fractal, int iterations);
    
    static void projectToLines(Figures& figures, Lines2D& lines, bool zBuff);
    static void projectToTriangles(Figures& figures, Triangles& triangles, Lines2D& extraLines, bool zBuff);
    static img::EasyImage drawLines(Lines2D& lines, int size, Color bgColor, bool zBuff, bool zBuffOut);
    static img::EasyImage drawTriangles(Triangles& triangles, Lines2D& extraLines, int size, Color bgColor, bool zBuff, bool zBuffOut);
    static img::EasyImage drawZBuffer(ZBuffer& zBuff);
    
    // Called by render2D.
    static img::EasyImage introColorRectangle(const ini::Configuration& config);
    static img::EasyImage introBlocks(const ini::Configuration& config);
    static img::EasyImage lSystem2D(const ini::Configuration& config);
    
    // Other static methods.
    static Color easyImageColor(std::vector<double> color);
    
public:
    static img::EasyImage render3D(const ini::Configuration& config, bool zBuff = false, bool zBuffOut = false, bool fill = false);
    static img::EasyImage render2D(const ini::Configuration& config);
};

#endif /* OPERATIONS_H_ */
