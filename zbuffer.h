/* 
 * File:   zbuffer.h
 * Author: Vreg
 *
 * Created on 25 maart 2014, 22:39
 */

#include <vector>

#ifndef ZBUFFER_H
#define	ZBUFFER_H

class ZBuffer : public std::vector<std::vector<double>>
{
public:
    ZBuffer(unsigned int width, unsigned int height);
};

#endif	/* ZBUFFER_H */

