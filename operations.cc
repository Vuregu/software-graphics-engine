/*
 * operations.cc
 *
 *  Created on: Feb 20, 2014
 *      Author: VregNamikaze
 */

#include "operations.h"
#include "math.h"
#include <iterator>
#include <cmath>
#include <stack>
#include <algorithm>
#include <limits>

#define M_PI 3.14159265358979323846

using namespace std;

img::EasyImage EngineOperations::render3D(const ini::Configuration& config, bool zBuff, bool zBuffOut, bool fill)
{
    int size = config["General"]["size"].as_int_or_die();
    Color bgColor = EngineOperations::easyImageColor(config["General"]["backgroundcolor"].as_double_tuple_or_die());
    int nrFigures = config["General"]["nrFigures"].as_int_or_die();
    vector<double> eyePoint = config["General"]["eye"].as_double_tuple_or_die();

    Figures figures;

    for (int i = 0; i < nrFigures; ++i)
    {
        stringstream figureName;
        figureName << "Figure" << i;
        
        string type = config[figureName.str()]["type"].as_string_or_die();
        
        Figure figure(EngineOperations::easyImageColor(config[figureName.str()]["color"].as_double_tuple_or_die()));
        
        double rotateX = config[figureName.str()]["rotateX"].as_double_or_die();
        double rotateY = config[figureName.str()]["rotateY"].as_double_or_die();
        double rotateZ = config[figureName.str()]["rotateZ"].as_double_or_die();
        double scale = config[figureName.str()]["scale"].as_double_or_die();
        vector<double> center = config[figureName.str()]["center"].as_double_tuple_or_die();
        
        if (type == "LineDrawing")
        {
            EngineOperations::linedrawing(figure, config, figureName);
        }
        else if (type == "Cube")
        {
            EngineOperations::generateCube(figure);
        }
        else if (type == "Tetrahedron")
        {
            EngineOperations::generateTetrahedron(figure);
        }
        else if (type == "Octahedron")
        {
            EngineOperations::generateOctahedron(figure);
        }
        else if (type == "Icosahedron")
        {
            EngineOperations::generateIcosahedron(figure);
        }
        else if (type == "BuckyBall")
        {
            EngineOperations::generateBuckyBall(figure);
        }
        else if (type == "Dodecahedron")
        {
            EngineOperations::generateDodecahedron(figure);
        }
        else if (type == "Sphere")
        {
            EngineOperations::generateSphere(figure, config[figureName.str()]["n"].as_int_or_die());
        }
        else if (type == "Cone")
        {
            EngineOperations::generateCone(figure, config[figureName.str()]["n"].as_int_or_die(), config[figureName.str()]["height"].as_double_or_die());
        }
        else if (type == "Cylinder")
        {
            EngineOperations::generateCylinder(figure, config[figureName.str()]["n"].as_int_or_die(), config[figureName.str()]["height"].as_double_or_die());
        }
        else if (type == "Torus")
        {
            EngineOperations::generateTorus(figure, config[figureName.str()]["r"].as_double_or_die(), config[figureName.str()]["R"].as_double_or_die(), config[figureName.str()]["n"].as_int_or_die(), config[figureName.str()]["m"].as_int_or_die());
        }
        else if (type == "3DLSystem")       
        {
            ifstream inputFile(&config[figureName.str()]["inputfile"].as_string_or_die()[0]);
            LParser::LSystem3D lSystem3D;
            
            inputFile >> lSystem3D;
            inputFile.close();
            
            EngineOperations::lSystem3D(figure, lSystem3D);
        }
        else if (type.substr(0, 7) == "Fractal")
        {
            if (type == "FractalCube")
            {
                EngineOperations::generateCube(figure);
            }
            else if (type == "FractalTetrahedron")
            {
                EngineOperations::generateTetrahedron(figure);
            }
            else if (type == "FractalOctahedron")
            {
                EngineOperations::generateOctahedron(figure);
            }
            else if (type == "FractalIcosahedron")
            {
                EngineOperations::generateIcosahedron(figure);
            }
            else if (type == "FractalBuckyBall")
            {
                EngineOperations::generateBuckyBall(figure);
            }
            else if (type == "FractalDodecahedron")
            {
                EngineOperations::generateDodecahedron(figure);
            }
            else
            {
                return img::EasyImage();
            }
            
            // Rotate.
            figure.rotateX(M_PI / 180 * rotateX);
            figure.rotateY(M_PI / 180 * rotateY);
            figure.rotateZ(M_PI / 180 * rotateZ);
            // Scale.
            figure.scaleFigure(scale);
            // Translate.
            figure.translate(Vector3D::vector(center[0], center[1], center[2]));

            // EyePoint transform.
            figure.eyePointTransform(Vector3D::point(eyePoint[0], eyePoint[1], eyePoint[2]));
            
            Figures fractal;
            EngineOperations::generateFractal(figure, fractal, config[figureName.str()]["nrIterations"].as_int_or_die(), config[figureName.str()]["fractalScale"].as_double_or_die());
            figures.insert(figures.end(), fractal.begin(), fractal.end());
            
            continue;
        }
        else if (type == "MengerSponge")
        { 
            Figures fractal;
            EngineOperations::generateMengerSponge(figure, fractal, config[figureName.str()]["nrIterations"].as_int_or_die());
            
            for (Figures::iterator fig = fractal.begin(); fig != fractal.end(); ++fig)
            {
                // Rotate.
                fig->rotateX(M_PI / 180 * rotateX);
                fig->rotateY(M_PI / 180 * rotateY);
                fig->rotateZ(M_PI / 180 * rotateZ);
                // Scale.
                fig->scaleFigure(scale);
                // Translate.
                fig->translate(Vector3D::vector(center[0], center[1], center[2]));

                // EyePoint transform.
                fig->eyePointTransform(Vector3D::point(eyePoint[0], eyePoint[1], eyePoint[2]));
            }
            
            figures.insert(figures.end(), fractal.begin(), fractal.end());
            
            continue;
        }
        else
        {
            return img::EasyImage();
        }
        
        // Rotate.
        figure.rotateX(M_PI / 180 * rotateX);
        figure.rotateY(M_PI / 180 * rotateY);
        figure.rotateZ(M_PI / 180 * rotateZ);
        // Scale.
        figure.scaleFigure(scale);
        // Translate.
        figure.translate(Vector3D::vector(center[0], center[1], center[2]));
        
        // EyePoint transform.
        figure.eyePointTransform(Vector3D::point(eyePoint[0], eyePoint[1], eyePoint[2]));

        figures.push_back(figure);
    }
    
    if (fill)
    {
        Triangles triangles;
        Lines2D extraLines;
        EngineOperations::projectToTriangles(figures, triangles, extraLines, zBuff);
        
        return EngineOperations::drawTriangles(triangles, extraLines, size, bgColor, zBuff, zBuffOut);
    }
    else
    {
        Lines2D lines;
        EngineOperations::projectToLines(figures, lines, zBuff);
        
        return EngineOperations::drawLines(lines, size, bgColor, zBuff, zBuffOut);
    }
}

void EngineOperations::linedrawing(Figure& figure, const ini::Configuration& config, stringstream& figureName)
{
    int nrPoints = config[figureName.str()]["nrPoints"].as_int_or_die();
    int nrLines = config[figureName.str()]["nrLines"].as_int_or_die();

    for (int i= 0; i< nrPoints; ++i)
    {
        stringstream pointName;
        pointName << "point" << i;

        vector<double> cords = config[figureName.str()][pointName.str()].as_double_tuple_or_die();
        figure.points.push_back(Vector3D::point(cords[0], cords[1], cords[2]));
    }

    Face face;

    for (int j = 0; j < nrLines; ++j)
    {
        stringstream lineName;
        lineName << "line" << j;

        vector<int> line = config[figureName.str()][lineName.str()].as_int_tuple_or_die();

        if (j == 0)
        {
            face.pointIndexes.push_back(line[0]);
            face.pointIndexes.push_back(line[1]);

            if (j == nrLines - 1)
            {
                figure.faces.push_back(face);
            }
        }
        else
        {                
            if (face.pointIndexes[face.pointIndexes.size() - 1] == line[0])
            {
                face.pointIndexes.push_back(line[1]);

                if (j == nrLines - 1)
                {
                    face.pointIndexes.pop_back();
                    figure.faces.push_back(face);
                }
            }
            else
            {
                if (face.pointIndexes.size() > 2)
                {
                    face.pointIndexes.pop_back();
                }

                figure.faces.push_back(face);
                face.pointIndexes.clear();
                face.pointIndexes.push_back(line[0]);
                face.pointIndexes.push_back(line[1]);

                if (j == nrLines - 1)
                {
                    figure.faces.push_back(face);
                }
            }
        }
    }
}

void EngineOperations::generateCube(Figure& figure)
{
    figure.points.push_back(Vector3D::point(1, -1, -1));
    figure.points.push_back(Vector3D::point(-1, 1, -1));
    figure.points.push_back(Vector3D::point(1, 1, 1));
    figure.points.push_back(Vector3D::point(-1, -1, 1));
    figure.points.push_back(Vector3D::point(1, 1, -1));
    figure.points.push_back(Vector3D::point(-1, -1, -1));
    figure.points.push_back(Vector3D::point(1, -1, 1));
    figure.points.push_back(Vector3D::point(-1, 1, 1));
    
    Face face;
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(4);
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(6);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(4);
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(7);
    face.pointIndexes.push_back(2);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(7);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(6);
    face.pointIndexes.push_back(3);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(6);
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(7);
    face.pointIndexes.push_back(3);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(4);
    figure.faces.push_back(face);
}

void EngineOperations::generateTetrahedron(Figure& figure)
{
    figure.points.push_back(Vector3D::point(1, -1, -1));
    figure.points.push_back(Vector3D::point(-1, 1, -1));
    figure.points.push_back(Vector3D::point(1, 1, 1));
    figure.points.push_back(Vector3D::point(-1, -1, 1));
    figure.points.push_back(Vector3D::point(-1, -1, 1));
    
    Face face;
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(2);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(2);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(1);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(2);
    figure.faces.push_back(face);
}

void EngineOperations::generateOctahedron(Figure& figure)
{
    figure.points.push_back(Vector3D::point(1, 0, 0));
    figure.points.push_back(Vector3D::point(0, 1, 0));
    figure.points.push_back(Vector3D::point(-1, 0, 0));
    figure.points.push_back(Vector3D::point(0, -1, 0));
    figure.points.push_back(Vector3D::point(0, 0, -1));
    figure.points.push_back(Vector3D::point(0, 0, 1));
    
    Face face;
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(5);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(5);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(5);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(5);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(4);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(4);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(4);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(4);
    figure.faces.push_back(face);
}

void EngineOperations::generateIcosahedron(Figure& figure)
{
    // Point 1.
    figure.points.push_back(Vector3D::point(0, 0, sqrt(5) / 2));
    // Points 2 - 6.
    for (int i = 2; i < 7; ++i)
    {
        figure.points.push_back(Vector3D::point(cos((i - 2) * 2 * M_PI / 5), sin((i - 2) * 2 * M_PI / 5), 0.5));
    }
    // Points 7 - 11.
    for (int i = 7; i < 12; ++i)
    {
        figure.points.push_back(Vector3D::point(cos(M_PI / 5 + (i - 7) * 2 * M_PI / 5), sin(M_PI / 5 + (i - 7) * 2 * M_PI / 5), -0.5));
    }
    // Point 12.
    figure.points.push_back(Vector3D::point(0, 0, -sqrt(5) / 2));   
    
    Face face;
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(2);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(3);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(4);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(4);
    face.pointIndexes.push_back(5);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(1);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(6);
    face.pointIndexes.push_back(2);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(6);
    face.pointIndexes.push_back(7);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(7);
    face.pointIndexes.push_back(3);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(7);
    face.pointIndexes.push_back(8);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(8);
    face.pointIndexes.push_back(4);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(4);
    face.pointIndexes.push_back(8);
    face.pointIndexes.push_back(9);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(4);
    face.pointIndexes.push_back(9);
    face.pointIndexes.push_back(5);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(9);
    face.pointIndexes.push_back(10);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(10);
    face.pointIndexes.push_back(1);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(10);
    face.pointIndexes.push_back(6);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(11);
    face.pointIndexes.push_back(7);
    face.pointIndexes.push_back(6);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(11);
    face.pointIndexes.push_back(8);
    face.pointIndexes.push_back(7);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(11);
    face.pointIndexes.push_back(9);
    face.pointIndexes.push_back(8);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(11);
    face.pointIndexes.push_back(10);
    face.pointIndexes.push_back(9);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(11);
    face.pointIndexes.push_back(6);
    face.pointIndexes.push_back(10);
    figure.faces.push_back(face);
}

void EngineOperations::generateBuckyBall(Figure& figure)
{
    vector<Vector3D> icosaPoints;
    
    // Point 1.
    icosaPoints.push_back(Vector3D::point(0, 0, sqrt(5) / 2));
    // Points 2 - 6.
    for (int i = 2; i < 7; ++i)
    {
        icosaPoints.push_back(Vector3D::point(cos((i - 2) * 2 * M_PI / 5), sin((i - 2) * 2 * M_PI / 5), 0.5));
    }
    // Points 7 - 11.
    for (int i = 7; i < 12; ++i)
    {
        icosaPoints.push_back(Vector3D::point(cos(M_PI / 5 + (i - 7) * 2 * M_PI / 5), sin(M_PI / 5 + (i - 7) * 2 * M_PI / 5), -0.5));
    }
    // Point 12.
    icosaPoints.push_back(Vector3D::point(0, 0, -sqrt(5) / 2));
    
    // First 5 hexagons.
    figure.points.push_back((2 * icosaPoints[0] + icosaPoints[1]) / 3);
    figure.points.push_back((icosaPoints[0] + 2 * icosaPoints[1]) / 3);
    figure.points.push_back((2 * icosaPoints[1] + icosaPoints[2]) / 3);
    figure.points.push_back((icosaPoints[1] + 2 * icosaPoints[2]) / 3);
    figure.points.push_back((2 * icosaPoints[2] + icosaPoints[0]) / 3);
    figure.points.push_back((icosaPoints[2] + 2 * icosaPoints[0]) / 3);

    Face face;
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(4);
    face.pointIndexes.push_back(5);
    figure.faces.push_back(face);

    figure.points.push_back((2 * icosaPoints[2] + icosaPoints[3]) / 3);
    figure.points.push_back((icosaPoints[2] + 2 * icosaPoints[3]) / 3);
    figure.points.push_back((2 * icosaPoints[3] + icosaPoints[0]) / 3);
    figure.points.push_back((icosaPoints[3] + 2 * icosaPoints[0]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(4);
    face.pointIndexes.push_back(6);
    face.pointIndexes.push_back(7);
    face.pointIndexes.push_back(8);
    face.pointIndexes.push_back(9);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[3] + icosaPoints[4]) / 3);
    figure.points.push_back((icosaPoints[3] + 2 * icosaPoints[4]) / 3);
    figure.points.push_back((2 * icosaPoints[4] + icosaPoints[0]) / 3);
    figure.points.push_back((icosaPoints[4] + 2 * icosaPoints[0]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(9);
    face.pointIndexes.push_back(8);
    face.pointIndexes.push_back(10);
    face.pointIndexes.push_back(11);
    face.pointIndexes.push_back(12);
    face.pointIndexes.push_back(13);
    figure.faces.push_back(face);

    figure.points.push_back((2 * icosaPoints[4] + icosaPoints[5]) / 3);
    figure.points.push_back((icosaPoints[4] + 2 * icosaPoints[5]) / 3);
    figure.points.push_back((2 * icosaPoints[5] + icosaPoints[0]) / 3);
    figure.points.push_back((icosaPoints[5] + 2 * icosaPoints[0]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(13);
    face.pointIndexes.push_back(12);
    face.pointIndexes.push_back(14);
    face.pointIndexes.push_back(15);
    face.pointIndexes.push_back(16);
    face.pointIndexes.push_back(17);
    figure.faces.push_back(face);

    figure.points.push_back((2 * icosaPoints[5] + icosaPoints[1]) / 3);
    figure.points.push_back((icosaPoints[5] + 2 * icosaPoints[1]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(17);
    face.pointIndexes.push_back(16);
    face.pointIndexes.push_back(18);
    face.pointIndexes.push_back(19);
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(0);
    figure.faces.push_back(face);
    
    // Pentagon between first 5 hexagons.
    face.pointIndexes.clear();
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(9);
    face.pointIndexes.push_back(13);
    face.pointIndexes.push_back(17);
    face.pointIndexes.push_back(0);
    figure.faces.push_back(face);
    
    // Second 5 hexagons.
    figure.points.push_back((2 * icosaPoints[11] + icosaPoints[7]) / 3);
    figure.points.push_back((icosaPoints[11] + 2 * icosaPoints[7]) / 3);
    figure.points.push_back((2 * icosaPoints[7] + icosaPoints[6]) / 3);
    figure.points.push_back((icosaPoints[7] + 2 * icosaPoints[6]) / 3);
    figure.points.push_back((2 * icosaPoints[6] + icosaPoints[11]) / 3);
    figure.points.push_back((icosaPoints[6] + 2 * icosaPoints[11]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(20);
    face.pointIndexes.push_back(21);
    face.pointIndexes.push_back(22);
    face.pointIndexes.push_back(23);
    face.pointIndexes.push_back(24);
    face.pointIndexes.push_back(25);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[11] + icosaPoints[8]) / 3);
    figure.points.push_back((icosaPoints[11] + 2 * icosaPoints[8]) / 3);
    figure.points.push_back((2 * icosaPoints[8] + icosaPoints[7]) / 3);
    figure.points.push_back((icosaPoints[8] + 2 * icosaPoints[7]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(26);
    face.pointIndexes.push_back(27);
    face.pointIndexes.push_back(28);
    face.pointIndexes.push_back(29);
    face.pointIndexes.push_back(21);
    face.pointIndexes.push_back(20);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[11] + icosaPoints[9]) / 3);
    figure.points.push_back((icosaPoints[11] + 2 * icosaPoints[9]) / 3);
    figure.points.push_back((2 * icosaPoints[9] + icosaPoints[8]) / 3);
    figure.points.push_back((icosaPoints[9] + 2 * icosaPoints[8]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(30);
    face.pointIndexes.push_back(31);
    face.pointIndexes.push_back(32);
    face.pointIndexes.push_back(33);
    face.pointIndexes.push_back(27);
    face.pointIndexes.push_back(26);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[11] + icosaPoints[10]) / 3);
    figure.points.push_back((icosaPoints[11] + 2 * icosaPoints[10]) / 3);
    figure.points.push_back((2 * icosaPoints[10] + icosaPoints[9]) / 3);
    figure.points.push_back((icosaPoints[10] + 2 * icosaPoints[9]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(34);
    face.pointIndexes.push_back(35);
    face.pointIndexes.push_back(36);
    face.pointIndexes.push_back(37);
    face.pointIndexes.push_back(31);
    face.pointIndexes.push_back(30);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[6] + icosaPoints[10]) / 3);
    figure.points.push_back((icosaPoints[6] + 2 * icosaPoints[10]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(25);
    face.pointIndexes.push_back(24);
    face.pointIndexes.push_back(38);
    face.pointIndexes.push_back(39);
    face.pointIndexes.push_back(35);
    face.pointIndexes.push_back(34);
    figure.faces.push_back(face);
    
    // Pentagon between second 5 hexagons.
    face.pointIndexes.clear();
    face.pointIndexes.push_back(20);
    face.pointIndexes.push_back(26);
    face.pointIndexes.push_back(30);
    face.pointIndexes.push_back(34);
    face.pointIndexes.push_back(25);
    figure.faces.push_back(face);
    
    // Third 5 hexagons.
    figure.points.push_back((2 * icosaPoints[1] + icosaPoints[6]) / 3);
    figure.points.push_back((icosaPoints[1] + 2 * icosaPoints[6]) / 3);
    figure.points.push_back((2 * icosaPoints[6] + icosaPoints[2]) / 3);
    figure.points.push_back((icosaPoints[6] + 2 * icosaPoints[2]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(40);
    face.pointIndexes.push_back(41);
    face.pointIndexes.push_back(42);
    face.pointIndexes.push_back(43);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(2);
    figure.faces.push_back(face);

    figure.points.push_back((2 * icosaPoints[7] + icosaPoints[2]) / 3);
    figure.points.push_back((icosaPoints[7] + 2 * icosaPoints[2]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(43);
    face.pointIndexes.push_back(42);
    face.pointIndexes.push_back(23);
    face.pointIndexes.push_back(22);
    face.pointIndexes.push_back(44);
    face.pointIndexes.push_back(45);
    figure.faces.push_back(face);

    figure.points.push_back((2 * icosaPoints[7] + icosaPoints[3]) / 3);
    figure.points.push_back((icosaPoints[7] + 2 * icosaPoints[3]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(45);
    face.pointIndexes.push_back(44);
    face.pointIndexes.push_back(46);
    face.pointIndexes.push_back(47);
    face.pointIndexes.push_back(7);
    face.pointIndexes.push_back(6);
    figure.faces.push_back(face);
    
    // Pentagon between third and first 5 hexagons.
    face.pointIndexes.clear();
    face.pointIndexes.push_back(6);
    face.pointIndexes.push_back(4);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(43);
    face.pointIndexes.push_back(45);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[3] + icosaPoints[7]) / 3);
    figure.points.push_back((icosaPoints[3] + 2 * icosaPoints[7]) / 3);
    figure.points.push_back((2 * icosaPoints[7] + icosaPoints[8]) / 3);
    figure.points.push_back((icosaPoints[7] + 2 * icosaPoints[8]) / 3);
    figure.points.push_back((2 * icosaPoints[8] + icosaPoints[3]) / 3);
    figure.points.push_back((icosaPoints[8] + 2 * icosaPoints[3]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(48);
    face.pointIndexes.push_back(49);
    face.pointIndexes.push_back(50);
    face.pointIndexes.push_back(51);
    face.pointIndexes.push_back(52);
    face.pointIndexes.push_back(53);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[3] + icosaPoints[8]) / 3);
    figure.points.push_back((icosaPoints[3] + 2 * icosaPoints[8]) / 3);
    figure.points.push_back((2 * icosaPoints[8] + icosaPoints[4]) / 3);
    figure.points.push_back((icosaPoints[8] + 2 * icosaPoints[4]) / 3);
    figure.points.push_back((2 * icosaPoints[4] + icosaPoints[3]) / 3);
    figure.points.push_back((icosaPoints[4] + 2 * icosaPoints[3]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(54);
    face.pointIndexes.push_back(55);
    face.pointIndexes.push_back(56);
    face.pointIndexes.push_back(57);
    face.pointIndexes.push_back(58);
    face.pointIndexes.push_back(59);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[4] + icosaPoints[8]) / 3);
    figure.points.push_back((icosaPoints[4] + 2 * icosaPoints[8]) / 3);
    figure.points.push_back((2 * icosaPoints[8] + icosaPoints[9]) / 3);
    figure.points.push_back((icosaPoints[8] + 2 * icosaPoints[9]) / 3);
    figure.points.push_back((2 * icosaPoints[9] + icosaPoints[4]) / 3);
    figure.points.push_back((icosaPoints[9] + 2 * icosaPoints[4]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(60);
    face.pointIndexes.push_back(61);
    face.pointIndexes.push_back(62);
    face.pointIndexes.push_back(63);
    face.pointIndexes.push_back(64);
    face.pointIndexes.push_back(65);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[4] + icosaPoints[9]) / 3);
    figure.points.push_back((icosaPoints[4] + 2 * icosaPoints[9]) / 3);
    figure.points.push_back((2 * icosaPoints[9] + icosaPoints[5]) / 3);
    figure.points.push_back((icosaPoints[9] + 2 * icosaPoints[5]) / 3);
    figure.points.push_back((2 * icosaPoints[5] + icosaPoints[4]) / 3);
    figure.points.push_back((icosaPoints[5] + 2 * icosaPoints[4]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(66);
    face.pointIndexes.push_back(67);
    face.pointIndexes.push_back(68);
    face.pointIndexes.push_back(69);
    face.pointIndexes.push_back(70);
    face.pointIndexes.push_back(71);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[5] + icosaPoints[9]) / 3);
    figure.points.push_back((icosaPoints[5] + 2 * icosaPoints[9]) / 3);
    figure.points.push_back((2 * icosaPoints[9] + icosaPoints[10]) / 3);
    figure.points.push_back((icosaPoints[9] + 2 * icosaPoints[10]) / 3);
    figure.points.push_back((2 * icosaPoints[10] + icosaPoints[5]) / 3);
    figure.points.push_back((icosaPoints[10] + 2 * icosaPoints[5]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(72);
    face.pointIndexes.push_back(73);
    face.pointIndexes.push_back(74);
    face.pointIndexes.push_back(75);
    face.pointIndexes.push_back(76);
    face.pointIndexes.push_back(77);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[5] + icosaPoints[10]) / 3);
    figure.points.push_back((icosaPoints[5] + 2 * icosaPoints[10]) / 3);
    figure.points.push_back((2 * icosaPoints[10] + icosaPoints[1]) / 3);
    figure.points.push_back((icosaPoints[10] + 2 * icosaPoints[1]) / 3);
    figure.points.push_back((2 * icosaPoints[1] + icosaPoints[5]) / 3);
    figure.points.push_back((icosaPoints[1] + 2 * icosaPoints[5]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(78);
    face.pointIndexes.push_back(79);
    face.pointIndexes.push_back(80);
    face.pointIndexes.push_back(81);
    face.pointIndexes.push_back(82);
    face.pointIndexes.push_back(83);
    figure.faces.push_back(face);
    
    figure.points.push_back((2 * icosaPoints[1] + icosaPoints[10]) / 3);
    figure.points.push_back((icosaPoints[1] + 2 * icosaPoints[10]) / 3);
    figure.points.push_back((2 * icosaPoints[10] + icosaPoints[6]) / 3);
    figure.points.push_back((icosaPoints[10] + 2 * icosaPoints[6]) / 3);
    figure.points.push_back((2 * icosaPoints[6] + icosaPoints[1]) / 3);
    figure.points.push_back((icosaPoints[6] + 2 * icosaPoints[1]) / 3);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(84);
    face.pointIndexes.push_back(85);
    face.pointIndexes.push_back(86);
    face.pointIndexes.push_back(87);
    face.pointIndexes.push_back(88);
    face.pointIndexes.push_back(89);
    figure.faces.push_back(face);
}

void EngineOperations::generateDodecahedron(Figure& figure)
{
    Figure icosa;
    EngineOperations::generateIcosahedron(icosa);
    
    for (unsigned int i = 0; i < icosa.faces.size(); ++i)
    {
        double x = (icosa.points[icosa.faces[i].pointIndexes[0]].x + icosa.points[icosa.faces[i].pointIndexes[1]].x + icosa.points[icosa.faces[i].pointIndexes[2]].x) / 3;
        double y = (icosa.points[icosa.faces[i].pointIndexes[0]].y + icosa.points[icosa.faces[i].pointIndexes[1]].y + icosa.points[icosa.faces[i].pointIndexes[2]].y) / 3;
        double z = (icosa.points[icosa.faces[i].pointIndexes[0]].z + icosa.points[icosa.faces[i].pointIndexes[1]].z + icosa.points[icosa.faces[i].pointIndexes[2]].z) / 3;
        figure.points.push_back(Vector3D::point(x, y, z));
    }
    
    Face face;
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(4);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(0);
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(6);
    face.pointIndexes.push_back(7);
    face.pointIndexes.push_back(1);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(1);
    face.pointIndexes.push_back(7);
    face.pointIndexes.push_back(8);
    face.pointIndexes.push_back(9);
    face.pointIndexes.push_back(2);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(2);
    face.pointIndexes.push_back(9);
    face.pointIndexes.push_back(10);
    face.pointIndexes.push_back(11);
    face.pointIndexes.push_back(3);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(3);
    face.pointIndexes.push_back(11);
    face.pointIndexes.push_back(12);
    face.pointIndexes.push_back(13);
    face.pointIndexes.push_back(4);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(4);
    face.pointIndexes.push_back(13);
    face.pointIndexes.push_back(14);
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(0);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(19);
    face.pointIndexes.push_back(18);
    face.pointIndexes.push_back(17);
    face.pointIndexes.push_back(16);
    face.pointIndexes.push_back(15);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(19);
    face.pointIndexes.push_back(14);
    face.pointIndexes.push_back(13);
    face.pointIndexes.push_back(12);
    face.pointIndexes.push_back(18);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(18);
    face.pointIndexes.push_back(12);
    face.pointIndexes.push_back(11);
    face.pointIndexes.push_back(10);
    face.pointIndexes.push_back(17);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(17);
    face.pointIndexes.push_back(10);
    face.pointIndexes.push_back(9);
    face.pointIndexes.push_back(8);
    face.pointIndexes.push_back(16);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(16);
    face.pointIndexes.push_back(8);
    face.pointIndexes.push_back(7);
    face.pointIndexes.push_back(6);
    face.pointIndexes.push_back(15);
    figure.faces.push_back(face);
    
    face.pointIndexes.clear();
    face.pointIndexes.push_back(15);
    face.pointIndexes.push_back(6);
    face.pointIndexes.push_back(5);
    face.pointIndexes.push_back(14);
    face.pointIndexes.push_back(19);
    figure.faces.push_back(face);
}

void EngineOperations::generateSphere(Figure& figure, int n)
{
    EngineOperations::generateIcosahedron(figure);
    
    for (int i = 0; i < n; ++i)
    {
        int fSize = figure.faces.size();
        for (int j = 0; j < fSize; ++j)
        {
            int facePos = j * 4;
            int p0 = figure.faces[facePos].pointIndexes[0];
            int p1 = figure.faces[facePos].pointIndexes[1];
            int p2 = figure.faces[facePos].pointIndexes[2];
            
            double x = (figure.points[p0].x + figure.points[p1].x) / 2;
            double y = (figure.points[p0].y + figure.points[p1].y) / 2;
            double z = (figure.points[p0].z + figure.points[p1].z) / 2;   
            figure.points.push_back(Vector3D::point(x, y, z));
            
            x = (figure.points[p1].x + figure.points[p2].x) / 2;
            y = (figure.points[p1].y + figure.points[p2].y) / 2;
            z = (figure.points[p1].z + figure.points[p2].z) / 2;  
            figure.points.push_back(Vector3D::point(x, y, z));
            
            x = (figure.points[p2].x + figure.points[p0].x) / 2;
            y = (figure.points[p2].y + figure.points[p0].y) / 2;
            z = (figure.points[p2].z + figure.points[p0].z) / 2;  
            figure.points.push_back(Vector3D::point(x, y, z));
            
            figure.faces[facePos].pointIndexes.clear();
            figure.faces[facePos].pointIndexes.push_back(p0);
            figure.faces[facePos].pointIndexes.push_back(figure.points.size() - 3);
            figure.faces[facePos].pointIndexes.push_back(figure.points.size() - 1);
        
            facePos++;
            figure.faces.insert(figure.faces.begin() + facePos, Face());
            figure.faces[facePos].pointIndexes.push_back(figure.points.size() - 3);
            figure.faces[facePos].pointIndexes.push_back(p1);
            figure.faces[facePos].pointIndexes.push_back(figure.points.size() - 2);
        
            facePos++;
            figure.faces.insert(figure.faces.begin() + facePos, Face());
            figure.faces[facePos].pointIndexes.push_back(figure.points.size() - 2);
            figure.faces[facePos].pointIndexes.push_back(p2);
            figure.faces[facePos].pointIndexes.push_back(figure.points.size() - 1);
        
            facePos++;
            figure.faces.insert(figure.faces.begin() + facePos, Face());
            figure.faces[facePos].pointIndexes.push_back(figure.points.size() - 3);
            figure.faces[facePos].pointIndexes.push_back(figure.points.size() - 2);
            figure.faces[facePos].pointIndexes.push_back(figure.points.size() - 1);
        }
    }
    
    for (unsigned int i = 0; i < figure.points.size(); ++i)
    {
        figure.points[i].normalise();
    }
}

void EngineOperations::generateCone(Figure& figure, int n, double height)
{
    Face finalFace;
    
    for (int i = 0; i < n; ++i)
    {
        figure.points.push_back(Vector3D::point(cos(2 * i * M_PI / n), sin(2 * i * M_PI / n), 0));
        
        figure.faces.push_back(Face());
        figure.faces[i].pointIndexes.push_back(i);
        figure.faces[i].pointIndexes.push_back((i + 1) % n);
        figure.faces[i].pointIndexes.push_back(n);
        
        finalFace.pointIndexes.push_back(i);
    }
    
    figure.points.push_back(Vector3D::point(0, 0, height));

    figure.faces.push_back(finalFace);
}

void EngineOperations::generateCylinder(Figure& figure, int n, double height)
{
    // Used to build up top and bottom faces.
    Face face;
    
    for (int i = 0; i < n; ++i)
    {
        // Middle faces.
        figure.faces.push_back(Face());
        figure.faces[i].pointIndexes.push_back(i);
        figure.faces[i].pointIndexes.push_back((i + 1) % n);
        figure.faces[i].pointIndexes.push_back(n + (i + 1) % n);
        figure.faces[i].pointIndexes.push_back(n + i);
        // Bottom points (z = 0).
        figure.points.push_back(Vector3D::point(cos(2 * i * M_PI / n), sin(2 * i * M_PI / n), 0));
        // Bottom face buildup.
        face.pointIndexes.push_back(i);
    }
    
    // Push bottom face.
    figure.faces.push_back(face);
    face.pointIndexes.clear();
    
    for (int i = 0; i < n; ++i)
    {
        // Top points (z = height).
        figure.points.push_back(Vector3D::point(cos(2 * i * M_PI / n), sin(2 * i * M_PI / n), height));
        // Top face buildup.
        face.pointIndexes.push_back(n + i);
    }
    
    // Push top face.
    figure.faces.push_back(face);
}

void EngineOperations::generateTorus(Figure& figure, double r, double R, int n, int m)
{
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
        /*/
        \*\ Generate point.
        /*/
            double u = 2 * i * M_PI / n;
            double v = 2 * j * M_PI / m;
            
            double x = (R + r * cos(v)) * cos(u);
            double y = (R + r * cos(v)) * sin(u);
            double z = r * sin(v);
            
            figure.points.push_back(Vector3D::point(x ,y, z));
        /*/
        \*\ Generate face.
        /*/
            Face face;
            face.pointIndexes.push_back(i * m + j);
            face.pointIndexes.push_back(((i + 1) % n) * m + j);
            face.pointIndexes.push_back(((i + 1) % n) * m + (j + 1) % m);
            face.pointIndexes.push_back(i * m + (j + 1) % m);
            figure.faces.push_back(face);
        }
    }
}

void EngineOperations::lSystem3D(Figure& figure, LParser::LSystem3D& lSystem3D)
{
    set<char> alphabet = lSystem3D.get_alphabet();
    string replacement[alphabet.size()];

    for (unsigned int i = 0; i < alphabet.size(); ++i)
    {
            replacement[i] = lSystem3D.get_replacement(*next(alphabet.begin(), i));
    }

    string systemString = lSystem3D.get_initiator();

    for (unsigned int i = 0; i < lSystem3D.get_nr_iterations(); ++i)
    {
            string temporaryString;

            for (unsigned int j = 0; j < systemString.size(); ++j)
            {
                    for (unsigned int k = 0; k < alphabet.size(); ++k)
                    {
                            if (*next(alphabet.begin(), k) == systemString[j])
                            {
                                    temporaryString.append(replacement[k]);
                                    break;
                            }
                            else if (k == alphabet.size() - 1)
                            {
                                    temporaryString.push_back(systemString[j]);
                            }
                    }
            }

            systemString = temporaryString;
    }
    
    double angle = M_PI / 180 * lSystem3D.get_angle();
    
    // Position.
    Vector3D currentPos = Vector3D::point(0, 0, 0);
    figure.points.push_back(currentPos);
    // Rotation and Direction.
    Vector3D f = Vector3D::vector(1, 0, 0);
    Vector3D u = Vector3D::vector(0, 0, 1);
    Vector3D l = Vector3D::vector(0, 1, 0);

    stack<vector<double>> brackets;

    for (unsigned int i = 0; i < systemString.size(); ++i)
    {
        // Turn left.
        if (systemString[i] == '+')
        {
            double cosA = cos(angle);
            double sinA = sin(angle);
            
            Vector3D fOld = f;
            Vector3D lOld = l;
            
            f = fOld * cosA + lOld * sinA;
            l = -fOld * sinA + lOld * cosA;
        }
        // Turn right.
        else if (systemString[i] == '-')
        {
            double cosA = cos(-angle);
            double sinA = sin(-angle);
            
            Vector3D fOld = f;
            Vector3D lOld = l;
            
            f = fOld * cosA + lOld * sinA;
            l = -fOld * sinA + lOld * cosA;
        }
        // Turn up.
        else if (systemString[i] == '^')
        {
            double cosA = cos(angle);
            double sinA = sin(angle);
            
            Vector3D fOld = f;
            Vector3D uOld = u;
            
            f = fOld * cosA + uOld * sinA;
            u = -fOld * sinA + uOld * cosA;
        }
        // Turn down.
        else if (systemString[i] == '&')
        {
            double cosA = cos(-angle);
            double sinA = sin(-angle);
            
            Vector3D fOld = f;
            Vector3D uOld = u;
            
            f = fOld * cosA + uOld * sinA;
            u = -fOld * sinA + uOld * cosA;
        }
        // Roll left.
        else if (systemString[i] == '\\')
        {
            double cosA = cos(angle);
            double sinA = sin(angle);
            
            Vector3D lOld = l;
            Vector3D uOld = u;
            
            l = lOld * cosA - uOld * sinA;
            u = lOld * sinA + uOld * cosA;
        }
        // Roll right.
        else if (systemString[i] == '/')
        {
            double cosA = cos(-angle);
            double sinA = sin(-angle);
            
            Vector3D lOld = l;
            Vector3D uOld = u;
            
            l = lOld * cosA - uOld * sinA;
            u = lOld * sinA + uOld * cosA;
        }
        // Turn around.
        else if (systemString[i] == '|')
        {
            f = -f;
            l = -l;
        }
        else if (systemString[i] == '(')
        {
            vector<double> posiAngle = {f.x, f.y, f.z, u.x, u.y, u.z, l.x, l.y, l.z, currentPos.x, currentPos.y, currentPos.z};
            brackets.push(posiAngle);
        }
        else if (systemString[i] == ')')
        {
            vector<double> posiAngle = brackets.top();
            brackets.pop();

            f.x = posiAngle[0];
            f.y = posiAngle[1];
            f.z = posiAngle[2];
            u.x = posiAngle[3];
            u.y = posiAngle[4];
            u.z = posiAngle[5];
            l.x = posiAngle[6];
            l.y = posiAngle[7];
            l.z = posiAngle[8];
            currentPos.x = posiAngle[9];
            currentPos.y = posiAngle[10];
            currentPos.z = posiAngle[11];
            
            figure.points.push_back(currentPos);
        }
        else
        {
            if (lSystem3D.draw(systemString[i]))
            {
                currentPos += f;
                figure.points.push_back(currentPos);
                Face face;
                face.pointIndexes.push_back(figure.points.size() - 2);
                face.pointIndexes.push_back(figure.points.size() - 1);
                figure.faces.push_back(face);
            }
            else
            {
                currentPos += f;
                figure.points.push_back(currentPos);
            }
        }
    }
}

void EngineOperations::generateFractal(Figure& figure, Figures& fractal, int iterations, double scale)
{
    fractal.push_back(figure);
    
    for (int i = 0; i < iterations; ++i)
    {
        Figures fractal_iterated;
        for (Figures::iterator fig_frac = fractal.begin(); fig_frac != fractal.end(); ++fig_frac)
        {
            for (int j = 0; j < fig_frac->points.size(); ++j)
            {
                fractal_iterated.push_back(*fig_frac);
                fractal_iterated.back().scaleFigure( 1 / scale );
                fractal_iterated.back().translate(fig_frac->points[j] - fractal_iterated.back().points[j]);
            }
        }
        fractal = fractal_iterated;
    }
}

void EngineOperations::generateMengerSponge(Figure& figure, Figures& fractal, int iterations)
{
    EngineOperations::generateCube(figure);
    fractal.push_back(figure);
    
    for (int i = 0; i < iterations; ++i)
    {
        Figures fractal_iterated;
        for (Figures::iterator fig_frac = fractal.begin(); fig_frac != fractal.end(); ++fig_frac)
        {
            double delta = ( fig_frac->points[0].x - fig_frac->points[1].x ) / 3;
            
            fractal_iterated.push_back(*fig_frac);
            for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            }
            fractal_iterated.back().translate(Vector3D::vector(-delta, -delta, delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(0, -delta, delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(delta, -delta, delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(-delta, -delta, 0));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(delta, -delta, 0));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(-delta, -delta, -delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(0, -delta, -delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(delta, -delta, -delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(-delta, 0, delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(delta, 0, delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(-delta, 0, -delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(delta, 0, -delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(-delta, delta, delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(0, delta, delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(delta, delta, delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(-delta, delta, 0));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(delta, delta, 0));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(-delta, delta, -delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(0, delta, -delta));
            fractal_iterated.push_back(*fig_frac);
                        for (int j = fractal_iterated.back().points.size(); j < fractal_iterated.back().points.size() - 1; ++j)
            {
                fractal_iterated.back().points[j].x *= 1.0 / 3.0;
                fractal_iterated.back().points[j].y *= 1.0 / 3.0;
                fractal_iterated.back().points[j].z *= 1.0 / 3.0;
            } fractal_iterated.back().translate(Vector3D::vector(delta, delta, -delta));
        }
        fractal = fractal_iterated;
    }
}

void EngineOperations::projectToLines(Figures& figures, Lines2D& lines, bool zBuff)
{
    Figures::iterator figure = figures.begin();
    for (figure; figure != figures.end(); ++figure)
    {
        vector<Point2D> projectedPoints;
        vector<int> zero_z_indexes;
        
        for (unsigned int i = 0; i < figure->points.size(); ++i)
        {
            if (figure->points[i].z != 0)
            {
                projectedPoints.push_back(Figure::projectPoint(figure->points[i], 1, zBuff));
            }
            else
            {
                projectedPoints.push_back(Point2D());
                zero_z_indexes.push_back(i);
            }
        }
        
        vector<Face>::iterator face = figure->faces.begin();
        for (face; face != figure->faces.end(); ++face)
        {
            // Filter out zero-z-points.
            vector<int> filteredPointIndexes;
            
            vector<int>::iterator pointIndex = face->pointIndexes.begin();
            for (pointIndex; pointIndex != face->pointIndexes.end(); ++pointIndex)
            {
                bool zeroZ;
                
                vector<int>::iterator zero_z_index = zero_z_indexes.begin(); 
                for (zero_z_index; zero_z_index != zero_z_indexes.end(); ++zero_z_index)
                {
                    if (*pointIndex == *zero_z_index)
                    {
                        zeroZ = true;
                        break;
                    }
                }
                
                if (!zeroZ) { filteredPointIndexes.push_back(*pointIndex); }
            }
            
            // Turn into lines.
            unsigned int pointsInFace = filteredPointIndexes.size();
            if (pointsInFace > 2)
            {
                for (unsigned int i = 0; i < pointsInFace; ++i)
                {
                    if (i != pointsInFace - 1)
                    {
                        lines.push_back(Line2D(projectedPoints[filteredPointIndexes[i]], projectedPoints[filteredPointIndexes[i + 1]], figure->color));
                    }
                    else
                    {
                        lines.push_back(Line2D(projectedPoints[filteredPointIndexes[i]], projectedPoints[filteredPointIndexes[0]], figure->color));
                    }
                }
            }
            else if (pointsInFace == 2)
            {
                lines.push_back(Line2D(projectedPoints[face->pointIndexes[0]], projectedPoints[face->pointIndexes[1]], figure->color));
            }
            else if (pointsInFace == 1)
            {
                lines.push_back(Line2D(projectedPoints[face->pointIndexes[0]], projectedPoints[face->pointIndexes[0]], figure->color));
            }
        }
    }
}

void EngineOperations::projectToTriangles(Figures& figures, Triangles& triangles, Lines2D& extraLines, bool zBuff)
{
    Figures::iterator figure = figures.begin();
    for (figure; figure != figures.end(); ++figure)
    {
        vector<int> zero_z_indexes;
        
        for (unsigned int i = 0; i < figure->points.size(); ++i)
        {
            if (figure->points[i].z == 0)
            {
                zero_z_indexes.push_back(i);
            }
        }
        
        vector<Face>::iterator face = figure->faces.begin();
        for (face; face != figure->faces.end(); ++face)
        {
            // Filter out zero-z-points.
            vector<int> filteredPointIndexes;
            
            vector<int>::iterator pointIndex = face->pointIndexes.begin();
            for (pointIndex; pointIndex != face->pointIndexes.end(); ++pointIndex)
            {
                bool zeroZ;
                
                vector<int>::iterator zero_z_index = zero_z_indexes.begin(); 
                for (zero_z_index; zero_z_index != zero_z_indexes.end(); ++zero_z_index)
                {
                    if (*pointIndex == *zero_z_index)
                    {
                        zeroZ = true;
                        break;
                    }
                }
                
                if (!zeroZ) { filteredPointIndexes.push_back(*pointIndex); }
            }
            
            // Triangles will be drawn as triangles.
            // Higher polygons will get triangulated first, then drawn as triangles.
            unsigned int pointsInFace = filteredPointIndexes.size();
            if (pointsInFace > 2)
            {
                for (unsigned int i = 1; i < pointsInFace - 1; ++i)
                {
                    Vector3D p1 = figure->points[filteredPointIndexes[0]];
                    Vector3D p2 = figure->points[filteredPointIndexes[i]];
                    Vector3D p3 = figure->points[filteredPointIndexes[i + 1]];
                    
                    triangles.push_back(Triangle(p1, p2, p3, figure->color));
                    triangles.back().projectTriangle(1, zBuff);
                }
            }
            // Anything less than a triangle will be drawn as a line.
            else if (pointsInFace == 2)
            {
                Point2D p1 = Figure::projectPoint(figure->points[face->pointIndexes[0]], 1, zBuff);
                Point2D p2 = Figure::projectPoint(figure->points[face->pointIndexes[1]], 1, zBuff);
                extraLines.push_back(Line2D(p1, p2, figure->color));
            }
            // Even if it's just a point.
            else if (pointsInFace == 1)
            {
                Point2D p1 = Figure::projectPoint(figure->points[face->pointIndexes[0]], 1, zBuff);
                Point2D p2 = Figure::projectPoint(figure->points[face->pointIndexes[0]], 1, zBuff);
                extraLines.push_back(Line2D(p1, p2, figure->color));
            }
        }
    }
}

img::EasyImage EngineOperations::drawLines(Lines2D& lines, int size, Color bgColor, bool zBuff, bool zBuffOut)
{
	double xMin = 0;
	double xMax = 0;
	double yMin = 0;
	double yMax = 0;
        
	for (unsigned int i = 0; i < lines.size(); ++i)
	{
            xMin = min(xMin, min(lines[i].p1.x, lines[i].p2.x));
            xMax = max(xMax, max(lines[i].p1.x, lines[i].p2.x));
            yMin = min(yMin, min(lines[i].p1.y, lines[i].p2.y));
            yMax = max(yMax, max(lines[i].p1.y, lines[i].p2.y));
	}

	double xRange = xMax - xMin;
	double yRange = yMax - yMin;
        
        unsigned int width = Math::doubleToInt((xRange / max(xRange, yRange)) * size);
        unsigned int height = Math::doubleToInt((yRange / max(xRange, yRange)) * size);
	img::EasyImage render(width, height, bgColor);
        
	double scaleFactor = render.get_width() / xRange * 0.95;
        
	double offsetX = render.get_width() / 2 - (xMin + xMax) / 2 * scaleFactor;
	double offsetY = render.get_height() / 2 - (yMin + yMax) / 2 * scaleFactor;
        
        ZBuffer zBuffer(width, height);
        
	for (unsigned int i = 0; i < lines.size(); ++i)
	{
            // Apply scale factor and offsets to projected points.
            lines[i].p1.x *= scaleFactor; lines[i].p1.y *= scaleFactor;
            lines[i].p2.x *= scaleFactor; lines[i].p2.y *= scaleFactor;

            lines[i].p1.x += offsetX; lines[i].p1.y += offsetY;
            lines[i].p2.y += offsetY; lines[i].p2.x += offsetX;
            
            if (zBuff)
            {
                render.draw_zbuff_line(lines[i], zBuffer);
            }
            else
            {
                render.draw_line(lines[i]);
            }
	}
        
        if (zBuffOut)
        {
            return EngineOperations::drawZBuffer(zBuffer);
        }
        
	return render;
}

img::EasyImage EngineOperations::drawTriangles(Triangles& triangles, Lines2D& extraLines, int size, Color bgColor, bool zBuff, bool zBuffOut)
{
	double xMin = 0;
	double xMax = 0;
	double yMin = 0;
	double yMax = 0;
        
	for (unsigned int i = 0; i < triangles.size(); ++i)
	{
            xMin = min(min(xMin, triangles[i].proj_p1.x), min(triangles[i].proj_p2.x, triangles[i].proj_p3.x));
            xMax = max(max(xMax, triangles[i].proj_p1.x), max(triangles[i].proj_p2.x, triangles[i].proj_p3.x));
            yMin = min(min(yMin, triangles[i].proj_p1.y), min(triangles[i].proj_p2.y, triangles[i].proj_p3.y));
            yMax = max(max(yMax, triangles[i].proj_p1.y), max(triangles[i].proj_p2.y, triangles[i].proj_p3.y));
	}
        
        for (unsigned int i = 0; i < extraLines.size(); ++i)
	{
            xMin = min(xMin, min(extraLines[i].p1.x, extraLines[i].p2.x));
            xMax = max(xMax, max(extraLines[i].p1.x, extraLines[i].p2.x));
            yMin = min(yMin, min(extraLines[i].p1.y, extraLines[i].p2.y));
            yMax = max(yMax, max(extraLines[i].p1.y, extraLines[i].p2.y));
	}
        
	double xRange = xMax - xMin;
	double yRange = yMax - yMin;
        
        unsigned int width = Math::doubleToInt((xRange / max(xRange, yRange)) * size);
        unsigned int height = Math::doubleToInt((yRange / max(xRange, yRange)) * size);
	img::EasyImage render(width, height, bgColor);
        
	double scaleFactor = render.get_width() / xRange * 0.95;
        
	double offsetX = render.get_width() / 2 - (xMin + xMax) / 2 * scaleFactor;
	double offsetY = render.get_height() / 2 - (yMin + yMax) / 2 * scaleFactor;
        
        ZBuffer zBuffer(width, height);
        
	for (unsigned int i = 0; i < triangles.size(); ++i)
	{
            /*/ Special case that can't be drawn as a triangle.
            \*\ Draw as line instead.
            /*/ if (triangles[i].proj_p1.y == triangles[i].proj_p2.y == triangles[i].proj_p3.y)
                {
                    Vector3D min_p = Vector3D::point(std::min(triangles[i].p1.x, std::min(triangles[i].p2.x, triangles[i].p3.x)), std::min(triangles[i].p2.y, std::min(triangles[i].p2.y, triangles[i].p3.y)), std::min(triangles[i].p1.z, std::min(triangles[i].p2.z, triangles[i].p3.z)));
                    Vector3D max_p = Vector3D::point(std::max(triangles[i].p1.x, std::max(triangles[i].p2.x, triangles[i].p3.x)), std::max(triangles[i].p1.y, std::max(triangles[i].p2.y, triangles[i].p3.y)), std::max(triangles[i].p1.z, std::max(triangles[i].p2.z, triangles[i].p3.z)));
                    Point2D proj_min_p = Figure::projectPoint(min_p, 1, zBuff);
                    Point2D proj_max_p = Figure::projectPoint(max_p, 1, zBuff);
                    Line2D line(proj_min_p, proj_max_p, triangles[i].color);

                    // Apply scale factor and offsets to projected points.
                    line.p1.x *= scaleFactor; line.p1.y *= scaleFactor;
                    line.p2.x *= scaleFactor; line.p2.y *= scaleFactor;

                    line.p1.x += offsetX; line.p1.y += offsetY;
                    line.p2.x += offsetX; line.p2.y += offsetY;

                    if (zBuff)
                    {
                        render.draw_zbuff_line(line, zBuffer);
                    }
                    else
                    {
                        render.draw_line(line);
                    }
                    
                    continue;
                }
            
            // Apply scale factor and offsets to projected points.
            triangles[i].proj_p1.x *= scaleFactor; triangles[i].proj_p1.y *= scaleFactor;
            triangles[i].proj_p2.x *= scaleFactor; triangles[i].proj_p2.y *= scaleFactor;
            triangles[i].proj_p3.x *= scaleFactor; triangles[i].proj_p3.y *= scaleFactor;

            triangles[i].proj_p1.x += offsetX; triangles[i].proj_p1.y += offsetY;
            triangles[i].proj_p2.x += offsetX; triangles[i].proj_p2.y += offsetY;
            triangles[i].proj_p3.x += offsetX; triangles[i].proj_p3.y += offsetY;
            
            if (zBuff)
            {
                render.draw_zbuff_triag(triangles[i], zBuffer, scaleFactor);
            }
            else
            {
                render.draw_triag(triangles[i]);
            }
	}
        
        for (unsigned int i = 0; i < extraLines.size(); ++i)
	{
            // Apply scale factor and offsets to projected points.
            extraLines[i].p1.x *= scaleFactor; extraLines[i].p1.y *= scaleFactor;
            extraLines[i].p2.x *= scaleFactor; extraLines[i].p2.y *= scaleFactor;

            extraLines[i].p1.x += offsetX; extraLines[i].p1.y += offsetY;
            extraLines[i].p2.y += offsetY; extraLines[i].p2.x += offsetX;
            
            if (zBuff)
            {
                render.draw_zbuff_line(extraLines[i], zBuffer);
            }
            else
            {
                render.draw_line(extraLines[i]);
            }
	}
        
        if (zBuffOut)
        {
            return EngineOperations::drawZBuffer(zBuffer);
        }
        
	return render;
}

img::EasyImage EngineOperations::drawZBuffer(ZBuffer& zBuff)
{
    double zMin;
    double zMax;
    double posInf = numeric_limits<double>::infinity();
    
    // Set initial zMin and zMax.
    for (int i = 0; i < zBuff.size(); ++i)
    {   
        for (int j = 0; j < zBuff[i].size(); ++j)
        {
            if (zBuff[i][j] != posInf)
            {
                zMin = zBuff[i][j];
                zMax = zBuff[i][j];
                goto finish;
            }
        }
     }
    finish:
    
    // Now actually find zMin and zMax.
    for (int i = 0; i < zBuff.size(); ++i)
    {   
        for (int j = 0; j < zBuff[i].size(); ++j)
        {
            if (zBuff[i][j] != posInf)
            {
                zMin = min(zMin, zBuff[i][j]);
                zMax = max(zMax, zBuff[i][j]);
            }
        }
    }
    
    double midChange = (zMin + zMax) / 2 - zMin;
    double f = 127.5F / midChange;
    
    img::EasyImage zBuffOut(zBuff[0].size(), zBuff.size(), Color(0, 0, 0));
    
    for (int i = 0; i < zBuff.size(); ++i)
    {   
        for (int j = 0; j < zBuff[i].size(); ++j)
        {
            if (zBuff[i][j] != posInf)
            {
                zBuffOut(j, i).blue = 255 - Math::doubleToInt((zBuff[i][j] - zMin) * f);
                zBuffOut(j, i).green = 255 - Math::doubleToInt((zBuff[i][j] - zMin) * f);
            }
        }
    }
    
    return zBuffOut;
}

img::EasyImage EngineOperations::render2D(const ini::Configuration& config)
{
        string type = config["General"]["type"].as_string_or_die();
        
        if (type == "IntroColorRectangle")
        {
            return EngineOperations::introColorRectangle(config);
        }
        else if (type == "IntroBlocks")
        {
            return EngineOperations::introBlocks(config);
        }
        else if (type == "2DLSystem")
        {
            return EngineOperations::lSystem2D(config);
        }
}

img::EasyImage EngineOperations::introColorRectangle(const ini::Configuration& config)
{
	int imgWidth = config["ImageProperties"]["width"].as_int_or_die();
	int imgHeight = config["ImageProperties"]["height"].as_int_or_die();

	img::EasyImage img(imgWidth, imgHeight);

	for (int i = 0; i < imgHeight; ++i)
	{
		for (int j = 0; j < imgWidth; ++j)
		{
			int r = j - ((int)j / 256) * 256;
			int g = i - ((int)i / 256) * 256;
			int b = (r + g) % 256;

			img(j, i).red = r;
			img(j, i).green = g;
			img(j, i).blue = b;
		}
	}

	return img;
}

img::EasyImage EngineOperations::introBlocks(const ini::Configuration& config)
{
	bool invertColors = config["BlockProperties"]["invertColors"].as_bool_or_die();

	int imgWidth = config["ImageProperties"]["width"].as_int_or_die();
	int imgHeight = config["ImageProperties"]["height"].as_int_or_die();
	int nrXBlocks = config["BlockProperties"]["nrXBlocks"].as_int_or_die();
	int nrYBlocks = config["BlockProperties"]["nrYBlocks"].as_int_or_die();
	int xBlockLenght = imgWidth / nrXBlocks;
	int yBlockLenght = imgHeight / nrYBlocks;

	Color evenColor = EngineOperations::easyImageColor(config["BlockProperties"]["colorWhite"].as_double_tuple_or_die());
	Color unevenColor = EngineOperations::easyImageColor(config["BlockProperties"]["colorBlack"].as_double_tuple_or_die());

	img::EasyImage img(imgWidth, imgHeight);

	for (int i = 0; i < imgHeight; ++i)
	{
		for (int j = 0; j < imgWidth; ++j)
		{
			int blockMode = (j / xBlockLenght + i / yBlockLenght) % 2;

			if (!invertColors)
			{
				switch (blockMode)
				{
					case 0:
						img(j, i).red = evenColor.red;
						img(j, i).green = evenColor.green;
						img(j, i).blue = evenColor.blue;
						break;

					case 1:
						img(j, i).red = unevenColor.red;
						img(j, i).green = unevenColor.green;;
						img(j, i).blue = unevenColor.blue;
						break;
				}
			}
			else
			{
				switch (blockMode)
				{
					case 0:
						img(j, i).red = unevenColor.red;
						img(j, i).green = unevenColor.green;
						img(j, i).blue = unevenColor.blue;
						break;

					case 1:
						img(j, i).red = evenColor.red;
						img(j, i).green = evenColor.green;
						img(j, i).blue = evenColor.blue;
						break;
				}
			}
		}
	}

	return img;
}

img::EasyImage EngineOperations::lSystem2D(const ini::Configuration& config)
{
	int size = config["General"]["size"].as_int_or_die();
	Color bgColor = EngineOperations::easyImageColor(config["General"]["backgroundcolor"].as_double_tuple_or_die());
	Color color = EngineOperations::easyImageColor(config["2DLSystem"]["color"].as_double_tuple_or_die());

	ifstream inputFile(&config["2DLSystem"]["inputfile"].as_string_or_die()[0]);

	LParser::LSystem2D lSystem2D;

	inputFile >> lSystem2D;
	inputFile.close();

	set<char> alphabet = lSystem2D.get_alphabet();
	string replacement[alphabet.size()];

	for (unsigned int i = 0; i < alphabet.size(); ++i)
	{
		replacement[i] = lSystem2D.get_replacement(*next(alphabet.begin(), i));
	}

	string systemString = lSystem2D.get_initiator();

	for (unsigned int i = 0; i < lSystem2D.get_nr_iterations(); ++i)
	{
		string temporaryString;

		for (unsigned int j = 0; j < systemString.size(); ++j)
		{
			for (unsigned int k = 0; k < alphabet.size(); ++k)
			{
				if (*next(alphabet.begin(), k) == systemString[j])
				{
					temporaryString.append(replacement[k]);
					break;
				}
				else if (k == alphabet.size() - 1)
				{
					temporaryString.push_back(systemString[j]);
				}
			}
		}

		systemString = temporaryString;
	}

	double angle = M_PI / 180 * lSystem2D.get_angle();
	double currentAngle = M_PI / 180 * lSystem2D.get_starting_angle();
	Point2D currentPos(0, 0);

	Lines2D lines;
	stack<vector<double>> brackets;

	for (unsigned int i = 0; i < systemString.size(); ++i)
	{
		if (systemString[i] == '+')
		{
			currentAngle += angle;
		}
		else if (systemString[i] == '-')
		{
			currentAngle -= angle;
		}
		else if (systemString[i] == '(')
		{
			vector<double> posiAngle = {currentAngle, currentPos.x, currentPos.y};
			brackets.push(posiAngle);
		}
		else if (systemString[i] == ')')
		{
			vector<double> posiAngle = brackets.top();
			brackets.pop();

			currentAngle = posiAngle[0];
			currentPos.x = posiAngle[1];
			currentPos.y = posiAngle[2];
		}
		else
		{
			if (lSystem2D.draw(systemString[i]))
			{
				Point2D newPos(currentPos.x + cos(currentAngle), currentPos.y + sin(currentAngle));
				Line2D line(currentPos, newPos, color);
				lines.push_back(line);
				currentPos = newPos;
			}
			else
			{
				currentPos.x += cos(currentAngle);
				currentPos.y += sin(currentAngle);
			}
		}
	}
        
        return EngineOperations::drawLines(lines, size, bgColor, false, false);
}

Color EngineOperations::easyImageColor(vector<double> color)
{
	return Color((unsigned int)(color[0] * 255), (unsigned int)(color[1] * 255), (unsigned int)(color[2] * 255));
}