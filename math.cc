/* 
 * File:   math.cc
 * Author: Vreg
 *
 * Created on 25 mei 2014, 5:07
 */

#include <cmath>
#include "math.h"

int Math::doubleToInt(double val)
{
    return floor(val + 0.5);
}

unsigned int Math::doubleToUInt(double val)
{
    return floor(val + 0.5);
}