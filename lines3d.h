/* 
 * File:   lines3d.h
 * Author: Vreg
 *
 * Created on 8 maart 2014, 20:35
 */

#ifndef LINES3D_H
#define	LINES3D_H

#include <vector>
#include "utils/vector.hh"
#include "lines2d.h"

class Face
{
public:
    std::vector<int> pointIndexes;
    
    Face(){};
};

class Figure
{
public:
    std::vector<Vector3D> points;
    std::vector<Face> faces;
    Color color;
    
    Figure(){};
    Figure(Color color);
    
    void scaleFigure(double scale);
    void rotateX(double angle);
    void rotateY(double angle);
    void rotateZ(double angle);
    void translate(Vector3D vector);
    void eyePointTransform(Vector3D eyePoint);
    void applyTransformation(const Matrix& trans);
    
    static void toPolar(Vector3D& point, double& radius, double& xAngle, double& zAngle);
    static Point2D projectPoint(Vector3D& point, double d, bool zBuff);
};

class Triangle
{
public:
        Vector3D p1;
        Vector3D p2;
        Vector3D p3;
        
        Point2D proj_p1;
        Point2D proj_p2;
        Point2D proj_p3;
        
	Color color;

	Triangle(Vector3D& p1, Vector3D& p2, Vector3D& p3, Color& color);
        void projectTriangle(double d, bool zBuff);
};

#endif	/* LINES3D_H */