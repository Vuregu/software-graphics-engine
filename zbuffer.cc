#include "zbuffer.h"
#include <limits>

ZBuffer::ZBuffer(unsigned int width, unsigned int height)
{
    double posInf = std::numeric_limits<double>::infinity();
    
    for (unsigned i = 0; i < height; ++i)
    {
        vector<double> row;
        
        for (unsigned j = 0; j < width; ++j)
        {
            row.push_back(posInf);
        }
        
        push_back(row);
    }
}
