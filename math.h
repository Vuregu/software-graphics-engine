/* 
 * File:   math.h
 * Author: Vreg
 *
 * Created on 25 mei 2014, 5:03
 */

#ifndef MATH_H
#define	MATH_H

class Math
{
public:
    static int doubleToInt(double val);
    static unsigned int doubleToUInt(double val);
};

#endif	/* MATH_H */

